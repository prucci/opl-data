#!/usr/bin/env python3
# vim: set ts=8 sts=4 et sw=4 tw=99:
#
# Calculates the Wilks column of the provided CSV.
# New! Also calculates age-adjusted Wilks!
#
# Overwrites the input file in-place.
#

import oplcsv
import sys
from wilks import wilks, mcculloch


def to_string(f):
    try:
        return "{:.2f}".format(f)
    except ValueError:
        print("Field not a float: %f" % f, file=sys.stderr)
        sys.exit(1)


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def main(filename):
    csv = oplcsv.Csv(filename)

    # Certain columns have to exist for Wilks to be calculable.
    # Age is for the McCulloch/Foster total, but we'll include it.
    for col in ['Sex', 'Age', 'BodyweightKg', 'TotalKg']:
        if col not in csv.fieldnames:
            print(' Missing necessary column: %s' % col, file=sys.stderr)
            return 1

    # If a Wilks column doesn't exist currently, we can just create it.
    if 'Wilks' not in csv.fieldnames:
        csv.append_column('Wilks')

    # If a McCulloch column doesn't exist currently, we can just create it.
    if 'McCulloch' not in csv.fieldnames:
        csv.append_column('McCulloch')

    indexSex = csv.index('Sex')
    indexAge = csv.index('Age')
    indexBodyweight = csv.index('BodyweightKg')
    indexTotal = csv.index('TotalKg')
    indexWilks = csv.index('Wilks')
    indexMcCulloch = csv.index('McCulloch')

    for row in csv.rows:
        sex = row[indexSex]
        bodyweight = row[indexBodyweight]
        total = row[indexTotal]

        if sex not in ['M', 'F']:
            continue

        if not bodyweight:
            continue
        bodyweight = float(bodyweight)

        if not total:
            continue
        total = float(total)

        # Add the Wilks score to the row.
        score = wilks(sex == 'M', bodyweight, total)
        row[indexWilks] = to_string(score)

        # Calculate the age-adusted score.
        age = row[indexAge].replace('.5', '')  # Round down when unknown.
        if is_int(age):
            row[indexMcCulloch] = to_string(
                mcculloch(sex == 'M', int(age), bodyweight, total))
        else:
            # Better than just leaving it blank, when we have some data.
            row[indexMcCulloch] = row[indexWilks]

    csv.write_filename(filename)
    return 0


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(' Usage: %s csv' % sys.argv[0], file=sys.stderr)
        sys.exit(1)
    sys.exit(main(sys.argv[1]))
